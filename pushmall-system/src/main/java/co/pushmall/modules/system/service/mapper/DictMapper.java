package co.pushmall.modules.system.service.mapper;

import co.pushmall.base.BaseMapper;
import co.pushmall.modules.system.domain.Dict;
import co.pushmall.modules.system.service.dto.DictDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author Zheng Jie
 * @date 2019-04-10
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DictMapper extends BaseMapper<DictDTO, Dict> {

}
