package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author pushmall
 * @date 2019-10-06
 */
public interface PushMallUserRepository extends JpaRepository<PushMallUser, Integer>, JpaSpecificationExecutor {

    @Modifying
    @Query(value = "update pushmall_user set status = ?1 where uid = ?2", nativeQuery = true)
    void updateOnstatus(int status, int id);

    @Modifying
    @Query(value = "update pushmall_user set now_money = now_money + ?1 where uid = ?2", nativeQuery = true)
    void updateMoney(double money, int id);

    @Modifying
    @Query(value = "update pushmall_user set brokerage_price = brokerage_price+?1 where uid = ?2", nativeQuery = true)
    void incBrokeragePrice(double price, int id);

    @Modifying
    @Query(value = "update pushmall_user set is_pass = ?1, level = ?2, is_check = 1 where uid = ?3", nativeQuery = true)
    void updateOnIsPass(Integer isPass, Integer applyLevel, Integer uid);

    @Query(value = "select * from  pushmall_user where status = 1 and level = ?1", nativeQuery = true)
    List<PushMallUser> findByLevel(Integer id);
}
