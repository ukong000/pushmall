package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallUserRecharge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2020-03-02
 */
public interface PushMallUserRechargeRepository extends JpaRepository<PushMallUserRecharge, Integer>, JpaSpecificationExecutor<PushMallUserRecharge> {
    /**
     * 根据 OrderId 查询
     *
     * @param order_id /
     * @return /
     */
    PushMallUserRecharge findByOrderId(String order_id);
}
