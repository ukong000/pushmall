package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallStoreCart;
import co.pushmall.modules.shop.service.dto.CountDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author xwb
 * @date 2020-04-02
 */
public interface PushMallStoreCartRepository extends JpaRepository<PushMallStoreCart, Long>, JpaSpecificationExecutor {

    @Query(value = "SELECT t.cate_name as catename from pushmall_store_cart c  " +
            "LEFT JOIN pushmall_store_product p on c.product_id = p.id  " +
            "LEFT JOIN pushmall_store_category t on p.cate_id = t.id " +
            "WHERE c.is_pay = 1", nativeQuery = true)
    List<CountDto> findCateName();
}
