package co.pushmall.modules.shop.rest;

import cn.hutool.core.util.StrUtil;
import co.pushmall.aop.log.Log;
import co.pushmall.exception.BadRequestException;
import co.pushmall.modules.shop.domain.PushMallStoreCategory;
import co.pushmall.modules.shop.service.PushMallStoreCategoryService;
import co.pushmall.modules.shop.service.dto.PushMallStoreCategoryDTO;
import co.pushmall.modules.shop.service.dto.PushMallStoreCategoryQueryCriteria;
import co.pushmall.utils.OrderUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author pushmall
 * @date 2019-10-03
 */
@Api(tags = "商城:商品分类管理")
@RestController
@RequestMapping("api")
public class StoreCategoryController {


    private final PushMallStoreCategoryService pushMallStoreCategoryService;


    public StoreCategoryController(PushMallStoreCategoryService pushMallStoreCategoryService) {
        this.pushMallStoreCategoryService = pushMallStoreCategoryService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/PmStoreCategory/download")
    @PreAuthorize("@el.check('admin','cate:list')")
    public void download(HttpServletResponse response, PushMallStoreCategoryQueryCriteria criteria) throws IOException {
        pushMallStoreCategoryService.download(pushMallStoreCategoryService.queryAll(criteria), response);
    }


    @Log("查询商品分类")
    @ApiOperation(value = "查询商品分类")
    @GetMapping(value = "/PmStoreCategory")
    @PreAuthorize("@el.check('admin','YXSTORECATEGORY_ALL','YXSTORECATEGORY_SELECT')")
    public ResponseEntity getPushMallStoreCategorys(PushMallStoreCategoryQueryCriteria criteria, Pageable pageable) {

        List<PushMallStoreCategoryDTO> categoryDTOList = pushMallStoreCategoryService.queryAll(criteria);
        return new ResponseEntity(pushMallStoreCategoryService.buildTree(categoryDTOList), HttpStatus.OK);
    }

    @Log("新增商品分类")
    @ApiOperation(value = "新增商品分类")
    @PostMapping(value = "/PmStoreCategory")
    @PreAuthorize("@el.check('admin','YXSTORECATEGORY_ALL','YXSTORECATEGORY_CREATE')")
    public ResponseEntity create(@Validated @RequestBody PushMallStoreCategory resources) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        if (resources.getPid() > 0 && StrUtil.isBlank(resources.getPic())) {
            throw new BadRequestException("子分类图片必传");
        }

        resources.setAddTime(OrderUtil.getSecondTimestampTwo());
        return new ResponseEntity(pushMallStoreCategoryService.create(resources), HttpStatus.CREATED);
    }

    @Log("修改商品分类")
    @ApiOperation(value = "修改商品分类")
    @PutMapping(value = "/PmStoreCategory")
    @PreAuthorize("@el.check('admin','YXSTORECATEGORY_ALL','YXSTORECATEGORY_EDIT')")
    public ResponseEntity update(@Validated @RequestBody PushMallStoreCategory resources) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        if (resources.getPid() > 0 && StrUtil.isBlank(resources.getPic())) {
            throw new BadRequestException("子分类图片必传");
        }
        pushMallStoreCategoryService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除商品分类")
    @ApiOperation(value = "删除商品分类")
    @DeleteMapping(value = "/PmStoreCategory/{id}")
    @PreAuthorize("@el.check('admin','YXSTORECATEGORY_ALL','YXSTORECATEGORY_DELETE')")
    public ResponseEntity delete(@PathVariable String id) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        String[] ids = id.split(",");
        for (String newId : ids) {
            pushMallStoreCategoryService.delete(Integer.valueOf(newId));
        }
        return new ResponseEntity(HttpStatus.OK);
    }
}
