package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallStoreProductAttr;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

/**
 * @author pushmall
 * @date 2019-10-13
 */
public interface PushMallStoreProductAttrRepository extends JpaRepository<PushMallStoreProductAttr, Integer>, JpaSpecificationExecutor {

    @Modifying
    @Transactional
    @Query(value = "delete from pushmall_store_product_attr where product_id =?1", nativeQuery = true)
    void deleteByProductId(int id);


}
