package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallSystemStore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2020-03-03
 */
public interface PushMallSystemStoreRepository extends JpaRepository<PushMallSystemStore, Integer>, JpaSpecificationExecutor<PushMallSystemStore> {
}
