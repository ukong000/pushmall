package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallWechatUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-12-13
 */
public interface PushMallWechatUserRepository extends JpaRepository<PushMallWechatUser, Integer>, JpaSpecificationExecutor {
}
