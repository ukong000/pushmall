package co.pushmall.modules.shop.rest;

import cn.hutool.core.util.ObjectUtil;
import co.pushmall.aop.log.Log;
import co.pushmall.modules.shop.domain.PushMallUser;
import co.pushmall.modules.shop.service.PushMallSystemConfigService;
import co.pushmall.modules.shop.service.PushMallUserLevelService;
import co.pushmall.modules.shop.service.PushMallUserService;
import co.pushmall.modules.shop.service.dto.UserMoneyDTO;
import co.pushmall.modules.shop.service.dto.PushMallUserQueryCriteria;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author pushmall
 * @date 2019-10-06
 */
@Api(tags = "商城:会员管理")
@RestController
@RequestMapping("api")
public class MemberController {

    private final PushMallUserService pushMallUserService;
    private final PushMallSystemConfigService pushMallSystemConfigService;
    private final PushMallUserLevelService pushMallUserLevelService;

    public MemberController(PushMallUserService pushMallUserService, PushMallSystemConfigService pushMallSystemConfigService, PushMallUserLevelService pushMallUserLevelService) {
        this.pushMallUserService = pushMallUserService;
        this.pushMallSystemConfigService = pushMallSystemConfigService;
        this.pushMallUserLevelService = pushMallUserLevelService;
    }

    @Log("查询用户")
    @ApiOperation(value = "查询用户")
    @GetMapping(value = "/PmUser")
    @PreAuthorize("@el.check('admin','YXUSER_ALL','YXUSER_SELECT')")
    public ResponseEntity getPushMallUsers(PushMallUserQueryCriteria criteria, Pageable pageable) {
        if (ObjectUtil.isNotNull(criteria.getIsPromoter())) {
            if (criteria.getIsPromoter() == 1) {
                String key = pushMallSystemConfigService.findByKey("store_brokerage_statu")
                        .getValue();
                if (Integer.valueOf(key) == 2) {
                    return new ResponseEntity(null, HttpStatus.OK);
                }
            }
        }
        Map<String, Object> stringObjectMap = pushMallUserService.queryAll(criteria, pageable);
        return new ResponseEntity(stringObjectMap, HttpStatus.OK);
    }

    @Log("新增用户")
    @ApiOperation(value = "新增用户")
    @PostMapping(value = "/PmUser")
    @PreAuthorize("@el.check('admin','YXUSER_ALL','YXUSER_CREATE')")
    public ResponseEntity create(@Validated @RequestBody PushMallUser resources) {
        return new ResponseEntity(pushMallUserService.create(resources), HttpStatus.CREATED);
    }

    @Log("修改用户")
    @ApiOperation(value = "修改用户")
    @PutMapping(value = "/PmUser")
    @PreAuthorize("@el.check('admin','YXUSER_ALL','YXUSER_EDIT')")
    public ResponseEntity update(@Validated @RequestBody PushMallUser resources) {
        pushMallUserService.update(resources);
        pushMallUserLevelService.setUserLevel(resources.getUid(), resources.getPushMallSystemUserLevel().getId());
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除用户")
    @ApiOperation(value = "删除用户")
    @DeleteMapping(value = "/PmUser/{uid}")
    @PreAuthorize("@el.check('admin','YXUSER_ALL','YXUSER_DELETE')")
    public ResponseEntity delete(@PathVariable Integer uid) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        pushMallUserService.delete(uid);
        return new ResponseEntity(HttpStatus.OK);
    }

    @ApiOperation(value = "用户禁用启用")
    @PostMapping(value = "/PmUser/onStatus/{id}")
    public ResponseEntity onStatus(@PathVariable Integer id, @RequestBody String jsonStr) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        JSONObject jsonObject = JSON.parseObject(jsonStr);
        int status = Integer.valueOf(jsonObject.get("status").toString());
        pushMallUserService.onStatus(id, status);
        return new ResponseEntity(HttpStatus.OK);
    }

    @ApiOperation(value = "用户会员等级审核")
    @PostMapping(value = "/PmUser/onIsPass/{id}")
    public ResponseEntity onIsPass(@PathVariable Integer id, @RequestBody String jsonStr) {
        JSONObject jsonObject = JSON.parseObject(jsonStr);
        int isPass = Integer.valueOf(jsonObject.get("isPass").toString());
        int applyLevel = Integer.valueOf(jsonObject.get("applyLevel").toString());
        if (isPass == 1) {
            pushMallUserService.onIsPass(id, isPass, applyLevel);
            pushMallUserLevelService.setUserLevel(id, applyLevel);
            return new ResponseEntity(HttpStatus.OK);
        } else {
            applyLevel = 1;
            pushMallUserService.onIsPass(id, isPass, applyLevel);
            pushMallUserLevelService.setUserLevel(id, applyLevel);
            return new ResponseEntity(HttpStatus.OK);
        }
    }

    @ApiOperation(value = "修改余额")
    @PostMapping(value = "/PmUser/money")
    @PreAuthorize("@el.check('admin','YXUSER_ALL','YXUSER_EDIT')")
    public ResponseEntity updatePrice(@Validated @RequestBody UserMoneyDTO param) {
        pushMallUserService.updateMoney(param);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
