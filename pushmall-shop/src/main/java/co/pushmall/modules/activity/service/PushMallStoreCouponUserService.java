package co.pushmall.modules.activity.service;

import co.pushmall.modules.activity.domain.PushMallStoreCouponUser;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponUserDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponUserQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-11-10
 */
//@CacheConfig(cacheNames = "yxStoreCouponUser")
public interface PushMallStoreCouponUserService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallStoreCouponUserQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallStoreCouponUserDTO> queryAll(PushMallStoreCouponUserQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallStoreCouponUserDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallStoreCouponUserDTO create(PushMallStoreCouponUser resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallStoreCouponUser resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);
}
