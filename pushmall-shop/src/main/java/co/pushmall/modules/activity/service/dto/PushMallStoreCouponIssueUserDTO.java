package co.pushmall.modules.activity.service.dto;

import lombok.Data;

import java.io.Serializable;


/**
 * @author pushmall
 * @date 2019-11-09
 */
@Data
public class PushMallStoreCouponIssueUserDTO implements Serializable {

    private Integer id;

    // 领取优惠券用户ID
    private Integer uid;

    // 优惠券前台领取ID
    private Integer issueCouponId;

    // 领取时间
    private Integer addTime;
}
