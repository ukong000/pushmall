package co.pushmall.modules.activity.repository;

import co.pushmall.modules.activity.domain.PushMallStoreCouponIssue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-11-09
 */
public interface PushMallStoreCouponIssueRepository extends JpaRepository<PushMallStoreCouponIssue, Integer>, JpaSpecificationExecutor {
}
